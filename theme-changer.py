#!/usr/bin/env python3

import requests
import os
import sys
import fileinput

LIGHT ={'vim':[
            'sol-term',
            'github',
            'laederon',
            'newspaper'
            ], 
        'alacritty':[
            'pencil_light.yml',
            'solarized_light.yml', 
            'gruvbox_light.yml',
            'seagull.yml'
            ]
        }

DARK = {'vim':['ego', 'neonwave', 'vimbrant', 'seattle'],
        'alacritty':['dracula.yml']}

COLORS_PATH = os.path.expanduser(os.path.join(
            '~',
            'githubs',
            'programs',
            'alacritty-colorscheme',
            'alacritty-colorscheme.py'
            ))

VIM_PATH = os.path.expanduser(os.path.join(
    '~',
    'githubs',
    'dotfiles',
    'nvim',
    '.config',
    'nvim',
    'plugins.vimrc'))
     
def findAndReplace(filename, text_to_search, replacement):
    with fileinput.FileInput(filename, inplace=True) as file:
        for line in file:
            if text_to_search in line:
                print(line.replace(line, replacement), end='')
            else:
                print(line, end='')


if __name__=='__main__':

    option = None
    try:
        option = sys.argv[1]
    except:
        pass
    
    if "l" == option:
        alacrittyArgs = ['-t', LIGHT['alacritty'][0]]
        vimArgs = LIGHT['vim'][0] + '\n'

    else:
        alacrittyArgs = ['-t', DARK['alacritty'][0]]
        vimArgs = DARK['vim'][3] + '\n'
    
    os.system(
            COLORS_PATH
            + ' '
            + alacrittyArgs[0]
            + ' ' + alacrittyArgs[1]
            )
    
    findAndReplace(
            VIM_PATH,
            'colorscheme ',
            'colorscheme '+ vimArgs
            )
