#!/usr/bin/env python3

wiki='Color-schemes.md'

def genFName(input):
   
    name = input.split('(')[-1]
    name = name.replace(')', '')
    name = name.lower()
    name = name.replace(' ', '_')
    return (name.strip('\n') + '.yml')

if __name__=='__main__':

    with open(wiki, 'r') as f:
        lines = f.readlines()

    begin = False
    for lineNumber, line in enumerate(lines):
        
        if '```\n' in line: begin = False

        if begin:
            with open(yamlName, 'a+') as f:
                f.write(line)

        if '```yaml' in line:
            begin = True            
            yamlName = genFName(lines[lineNumber+1])

        lineNumber += 1
